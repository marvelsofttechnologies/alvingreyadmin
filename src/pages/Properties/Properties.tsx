import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import Property from "../../classes/Property";
import AddProperty from "../../components/Properties/AddProperty";
import { PagedApiResponseData, PropertyView } from "../../utilities/Interfaces";
import Modal from "../../utilities/Modal";
import Naira from 'react-naira' ;

export default function Properties() {
  const [isAddPropertyModalOpen, setIsAddPropertyModalOpen] = useState(false);
  const [properties, setProperties] = useState<PropertyView[]>([]);
  const [totalPages,setTotalPages] = useState(0);
  const [search,setSearch] = useState("");
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [propertyId, setPropertyId] = useState(0)

  const searchProperties = async () => {
    const url = `?search=${search}`;
    listProperties(url);
  }

  const listProperties = async (url?:any) => {
    const data = await Property.List(url);
    if (data?.status) {
      let pagedData = data.data as unknown as PagedApiResponseData;
      let properties: PropertyView[] =
        pagedData.value as unknown as PropertyView[];
      console.log({ properties });
      setProperties(properties);
      setTotalPages(Math.floor(data.data.size / data.data.limit));
      return;
    }
    toast.error(data?.message);
  };

  const closeModal = async () => {
    setIsAddPropertyModalOpen(false);
    await listProperties();
  }

  const deleteProperty = async () => {
    try {
      let data = await Property.Delete(propertyId);
      if (data.status) {
        toast.success("Property deleted Successfully");
        setDeleteModalOpen(false);
        await listProperties();
        return;
      }
      toast.error("An error occurred");
    } catch (error) {
      console.error(error);
      toast.error("An error occurred");
    }
  };



  useEffect(() => {
    const getInitialData = async () => {
      await listProperties();
    };
    getInitialData();
  }, []);
  return (
    <>
      <Modal
        open={isAddPropertyModalOpen}
        onClose={async() => await closeModal()
          }
      >
        <AddProperty close={() => setIsAddPropertyModalOpen(false)} />
      </Modal>
      <Modal
        open={deleteModalOpen}
        onClose={() => {
          setIsAddPropertyModalOpen(false);
        }}
        height={'150px'}
        width={'30%'}
      >
        <div className="flex-column justify-content-center">
          <h5 >
            Are you sure you want to delete this property
          </h5>
          <div className="row  flex-center justify-space-between mt-3">
            <div className="col">
            <button
              className="user-red-btn-a "
              style={{ backgroundColor: "#000"}}
              onClick={async () => await setDeleteModalOpen(false)}
            >
              No
            </button>
            </div>
            <div className='col'>

          <button
            className="user-red-btn-a "
            onClick={async () => await deleteProperty()}
          >
            Yes
          </button>
            </div>
          </div>
        </div>
      </Modal>
      <div className="mt-4">
        <div className="user-content-box w-100">
          <div className="flex-item mb-4">
            <div className="admin-search-bar-2">
              <i className="far fa-search" />
              <input
                type="text"
                className="property-search"
                placeholder="Search"
                onChange={async(e) =>{
                   setSearch(e.target.value)
                  await searchProperties();
                  }}
              />
            </div>
            <button
              className="user-red-btn-a"
              onClick={() => setIsAddPropertyModalOpen(true)}
            >
              <i className="fas fa-plus" />
              Add New Property
            </button>
          </div>
          <div className="table-box">
            <table className="table table-hover">
              <thead className="ag-thead">
                <tr>
                  <th scope="col">Name</th>
                  <th scope="col">Type</th>
                  <th scope="col">Price</th>
                  <th scope="col">Title</th>
                  <th scope="col">Location</th>
                  <th scope="col">Bedrooms</th>
                  <th scope="col">Status</th>
                  <th scope="col">Delete</th>
                </tr>
              </thead>
              <tbody>
                {properties.map((property, index) => {
                  return (
                      <tr>
                        <Link to={`/properties/${property.id}`}>
                      <th scope="row">{property.name}</th>
                      </Link>
                      <td>{property.propertyType}</td>
                      <td><Naira>{property.price}</Naira></td>
                      <td>{property.title}</td>
                      <td>{property.address}</td>
                      <td>{property.numberOfBedrooms}</td>
                      <td>{property.status}</td>
                      <td className='text-center'><i className="fas fa-trash" onClick={() => { 
                        setPropertyId(property?.id ? property.id : 0);
                        setDeleteModalOpen(true);
                      }}></i></td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
          <div className="ag-space" />
          <div className="pagination">
            <div className="page-arrows">
              <i className="fal fa-angle-left" />
            </div>
            <div className="page-number">1</div>
            <div className="page-number nb">{`of`}</div>
            <div className="page-number">{totalPages === 0  ? "1" : `${totalPages}`}</div>
            <div className="page-arrows">
              <i className="fal fa-angle-right" />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
