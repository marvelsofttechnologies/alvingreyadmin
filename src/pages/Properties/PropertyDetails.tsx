import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router";
import { toast } from "react-toastify";
import Property from "../../classes/Property";
import EditProperty from "../../components/Properties/EditProperty";
import SingleeProperty from "../../components/Properties/SingleeProperty";
import Fetch from "../../utilities/Fetch";
import { PropertyView } from "../../utilities/Interfaces";


type PropertyParams = {
  id:string;
}

export default function PropertyDetails() {
  const history = useHistory();
  const { id } = useParams<PropertyParams>();
  const [property,setProperty] = useState<PropertyView>();
  
  const getProperty = async () => {
    let data = await Property.GetById(parseInt(id));
    if(!data.status){
      toast.error(data.message);
      return;
    }
    setProperty(data.data);
  }

  useEffect(() => {
    const getInitialData = async () => {
      await getProperty();
    }
    getInitialData();
  },[])

  return (
    <div className="mt-4">
    <div className="user-content-box w-100">
      <div className="container-fluid">
      <div className="flex-item mb-4">
        <h5 className="modal-title bold">
          <i className="fas fa-angle-left mod-lf" onClick={() => history.push('/properties')}></i>{property?.name}
        </h5>
        <i className="fas fa-times mod" onClick={() => history.push('/properties')}></i>
      </div>
        <div className="row">
          <EditProperty property={property && property != undefined ? property : {}}/>
          <SingleeProperty property={property}/>
        </div>
      </div>
    </div>
    </div>
  );
}
