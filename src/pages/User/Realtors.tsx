import React, { useEffect, useRef, useState } from "react";
import { toast } from "react-toastify";
import LoadingBar from "react-top-loading-bar";
import { User } from "../../classes/User";
import ListWithSearch from "../../components/Realtors/ListWithSearch";
import NetworkTab from "../../components/Realtors/NetworkTab";
import PermanentDetails from "../../components/Realtors/PermanentDetails";
import ProfileTab from "../../components/Realtors/ProfileTab";
import { UserView } from "../../utilities/Interfaces";

export default function Realtors() {
  const [currentTab, setCurrentTab] = useState("profile");
  const [realtors, setRealtors] = useState<UserView[]>();
  const [totalPages, setTotalPages] = useState(0);
  const [loading, setLoading] = useState(false);
  const [progress, setProgress] = useState(0);
  const [selectedUser,setSelectedUser] = useState<UserView>();
  const [pagingDetails,setPagingDetails] = useState<any>();
  const [currentPage,setCurrentPage] = useState(1);
  const [search,setSearch] = useState("");

  const list = async (url?: string) => {
    if(!url){
      setLoading(true);
    }
    setProgress(50);
    let data = await User.list(url);
    if (!data.status) {
      toast.error(data.message);
      setLoading(false);
      setProgress(100);
      return;
    }
    setTotalPages(Math.floor(data.data.size / data.data.limit));
    setCurrentPage((data.data.limit + data.data.offset )/ data.data.limit);
    setRealtors(data.data.value);
    setPagingDetails(data.data);
    setSelectedUser(data.data.value[0]);
    // toast.success(data.message);
    console.log(data);
    setLoading(false);
    setProgress(100);
    return;
  };
  const searchUsers = async () => {
    const url = `?search=${search}`;
    list(url);
  }

  const paginate = async (direction: any) => {
    console.log({pagingDetails});
    
    let url = "";
    if(direction == "next"){
      url = pagingDetails.next.href.split('list')[1];
      await list(url);
      return;
    }
    url = pagingDetails.previous.href.split('list')[1];
    await list(url);
    return;
  } 

  const setUser = (id: any) => {
    let selectedUser = realtors?.filter(record => record.id == id)[0];
    setSelectedUser(selectedUser);
  }

 const updateSearchString =async (char:any) => {
  setSearch(char);
  await searchUsers();
 }

  useEffect(() => {
    const fetchInitialData = async () => {
      await list();
    };
    fetchInitialData();
  },[]);


  return loading ? (
    <LoadingBar height={5} color={"red"} progress={progress} onLoaderFinished={() => setProgress(0)}/>
  ) : (
    <div className="admin-container mt-4">
      <div className="flex-unristricted align-items-start divided">
        <ListWithSearch
          realtors={realtors && realtors.length > 0 ? realtors : []}
          totalPages={totalPages}
          setUser={setUser}
          paginate={paginate}
          currentPage={currentPage}
          search={updateSearchString}
        />
        <div className="user-content-box">
          <PermanentDetails
            realtor={selectedUser}
            list={list}
          />
          <div className="admin-tab">
            <div
              className={
                currentTab === "profile"
                  ? "single-admin-tab current"
                  : "single-admin-tab"
              }
              onClick={() => setCurrentTab("profile")}
            >
              Profile
            </div>
            <div
              className={
                currentTab !== "profile"
                  ? "single-admin-tab current"
                  : "single-admin-tab"
              }
              onClick={() => setCurrentTab("network")}
            >
              Network
            </div>
          </div>
          {currentTab === "profile" ? (
            <ProfileTab
              realtor={selectedUser}
            />
          ) : (
            <NetworkTab
              realtor={selectedUser}
            />
          )}
        </div>
      </div>
    </div>
  );
}
