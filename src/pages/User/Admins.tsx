import React, { useEffect, useState } from "react";
import AddAdmin from "../../components/Admins/AddAdmin";
import PermissionsTab from "../../components/Admins/PermissionsTab";
import ProfileTabAdmin from "../../components/Admins/ProfileTabAdmin";
import ListWithSearch from "../../components/Realtors/ListWithSearch";
import { UserView } from "../../utilities/Interfaces";
import Modal from "../../utilities/Modal";
import { toast } from "react-toastify";
import LoadingBar from "react-top-loading-bar";
import { User } from "../../classes/User";

export default function Admins() {
    const [currentTab,setCurrentTab] = useState('profile');
    const[admins,setAdmins] = useState<UserView[]>();
    const [modalOpen,setModalOpen] = useState(false);
    const [progress, setProgress] = useState(0);
    const [totalPages, setTotalPages] = useState(0);
    const [loading, setLoading] = useState(false);
    const [selectedUser,setSelectedUser] = useState<UserView>();
    const [pagingDetails,setPagingDetails] = useState<any>();
    const [currentPage,setCurrentPage] = useState(1);
    const [search,setSearch] = useState("");
    const [deleteModalOpen, setDeleteModalOpen] = useState(false);

    const list = async (url?: string) => {
      if(!url){
        setLoading(true);
      }
      setProgress(50);
      let data = await User.listAdmin(url);
      if (!data.status) {
        toast.error(data.message);
        setLoading(false);
        setProgress(100);
        return;
      }
      setTotalPages(Math.floor(data.data.size / data.data.limit));
      setCurrentPage((data.data.limit + data.data.offset) / data.data.limit);
      setAdmins(data.data.value);
      setPagingDetails(data.data);
      setSelectedUser(data.data.value[0]);
      // toast.success(data.message);
      console.log(data);
      setLoading(false);
      setProgress(100);
      return;
    };

    const searchUsers = async () => {
      const url = `?search=${search}`;
      list(url);
    }
    const updateSearchString =async (char:any) => {
      setSearch(char);
      await searchUsers();
     }

    useEffect(() => {
      const fetchInitialData = async () => {
        await list();
      };
      fetchInitialData();
    },[]);

    const deleteUser = async (email?: string) => {
      try {
        let data = await User.delete(email);
        if (data.status) {
          toast.success("User deleted Successfully");
          await list();
          return;
        }
        toast.error("An error occurred");
      } catch (error) {
        console.error(error);
        toast.error("An error occurred");
      }
    };

    const paginate = async (direction: any) => {
      console.log({pagingDetails});
      
      let url = "";
      if(direction == "next"){
        url = pagingDetails.next.href.split('list')[1];
        await list(url);
        return;
      }
      url = pagingDetails.previous.href.split('list')[1];
      await list(url);
      return;
    } 

    const setUser = (id: any) => {
      let selectedUser = admins?.filter(record => record.id == id)[0];
      setSelectedUser(selectedUser);
    }

  return (
    loading ? <LoadingBar height={5} color={"red"} progress={progress} onLoaderFinished={() => setProgress(0)}/> :
    <>
    <Modal open={modalOpen} onClose={() => {setModalOpen(false)}}>
      <AddAdmin close={() => setModalOpen(false)}/>
    </Modal>
    <Modal
        open={deleteModalOpen}
        onClose={() => {
          setModalOpen(false);
        }}
        height={'150px'}
        width={'30%'}
      >
        <div className="flex-column justify-content-center">
          <h5 >
            Are you sure you want to delete <span>{selectedUser?.fullName} </span>
          </h5>
          <div className="row  flex-center justify-space-between mt-3">
            <div className="col">
            <button
              className="user-red-btn-a "
              style={{ backgroundColor: "#000"}}
              onClick={async () => await setDeleteModalOpen(false)}
            >
              No
            </button>
            </div>
            <div className='col'>

          <button
            className="user-red-btn-a "
            onClick={async () => await deleteUser(selectedUser?.email)}
          >
            Yes
          </button>
            </div>
          </div>
        </div>
      </Modal>
    <div className="admin-container mt-4">
      <div className="flex-unristricted align-items-start divided">
        <ListWithSearch search={updateSearchString} realtors={admins && admins.length > 0 ? admins : []} setUser={setUser}  totalPages={totalPages} currentPage={currentPage}/>
        <div className="user-content-box">
          <div className="flex-item">
            <div className="flex-unrestricted">
              <div className="username">
                <h5 className="user-name-a">{selectedUser?.fullName}</h5>
              </div>
            </div>
            <button
          className="user-red-btn-a"
          style={{ backgroundColor: "#000" }}
          onClick={async () => await setDeleteModalOpen(true)}
        >
          <i className="fas fa-trash" />
          Delete
        </button>
            <button className="user-red-btn-a" onClick={() => setModalOpen(true)}>
              <i className="fas fa-plus" onClick={() => setModalOpen(true)} />
              Add New Admin
            </button>
          </div>
          <div className="admin-tab">
            <div className={currentTab == 'profile' ? "single-admin-tab current": 'single-admin-tab'} onClick={() => setCurrentTab('profile')}>Profile</div>
            <div className={currentTab != 'profile' ? "single-admin-tab current": 'single-admin-tab'} onClick={() => setCurrentTab('permissions')}>Permissions</div>
          </div>
          {currentTab == "profile" ? <ProfileTabAdmin admin={selectedUser ? selectedUser : {}} /> : <PermissionsTab admin={selectedUser ? selectedUser : {}}/> }

        </div>
      </div>
    </div>
    </>
  );
}
