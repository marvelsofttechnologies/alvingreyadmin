import React, { useEffect, useRef, useState } from "react";
import { toast } from "react-toastify";
import LoadingBar from "react-top-loading-bar";
import { User } from "../../classes/User";

export default function Dashboard() {
  const [userCount, setUserCount] = useState(0);
  const [downlines, setDownlines] = useState(0);
  const [genderMetrics,setGenderMetrics] = useState({item1: 0,item2:0});
  const [loading, setLoading] = useState(false);
  const [progress, setProgress] = useState(0);
  const loaderRef = useRef(null);
  let loadingBar = null;

  const getUserCount = async () => {
    setLoading(true);
    const data = await User.count();
    if (data?.status) {
      setUserCount(data.data);
      setLoading(false);
      return;
    }
    toast.error(data.message);
    setLoading(false);
    return;
  };
  const getDownlinesCount = async () => {
    setLoading(true);
    //  @ts-ignore
    loaderRef.current.continuousStart();
    const data = await User.downlinesCount();
    if (data?.status) {
      setDownlines(data.data);
      setLoading(false);
      setProgress(100);
      return;
    }
    toast.error(data.message);
    setLoading(false);
    setProgress(100);
    return;
  };

  const getGenderMetrics = async () => {
    setLoading(true);
    //  @ts-ignore
    loaderRef.current.continuousStart();
    const data = await User.genderMetrics();
    if (data?.status) {
      setGenderMetrics(data.data)
      setLoading(false);
      setProgress(100);
      return;
    }
    toast.error(data.message);
    setLoading(false);
    setProgress(100);
    return;
  };

  useEffect(() => {
    const getInitialData = async () => {
      await getUserCount();
      await getGenderMetrics();
      await getDownlinesCount();
    };
    getInitialData();
  }, []);

  return loading ? (
    <LoadingBar
      height={3}
      color="red"
      ref={(loaderRef)}
      progress={progress}
    />
  ) : (
    <div className="admin-container">
      <div className="container-fluid">
        <div className="row">
          <div className="col-lg-8">
            <h5 className="admin-section">User</h5>
            <div className="admin-card-group">
              <div className="admin-card">
                <div className="flex-item">
                  <span className="card-name">All Users</span>
                  <span className="card-rise">+12%</span>
                </div>
                <h2 className="card-counts">{userCount}</h2>
              </div>
              <div className="admin-card">
                <div className="flex-item">
                  <span className="card-name">Male Users</span>
                  <span className="card-rise">+12%</span>
                </div>
                <h2 className="card-counts">{genderMetrics.item1}</h2>
              </div>
              <div className="admin-card">
                <div className="flex-item">
                  <span className="card-name">Female Users</span>
                  <span className="card-rise">+12%</span>
                </div>
                <h2 className="card-counts">{genderMetrics.item2}</h2>
              </div>
            </div>
            <h5 className="admin-section">Network</h5>
            <div className="admin-card-group">
              <div className="admin-card">
                <div className="flex-item">
                  <span className="card-name">Total Original Users</span>
                  <span className="card-rise">+12%</span>
                </div>
                <h2 className="card-counts">{userCount - downlines}</h2>
              </div>
              <div className="admin-card">
                <div className="flex-item">
                  <span className="card-name">Total Downlines</span>
                  <span className="card-rise">+12%</span>
                </div>
                <h2 className="card-counts">{downlines}</h2>
              </div>
            </div>
          </div>
          {/* <div className="col-lg-4">
            <h5 className="admin-section">Performance</h5>
            <div className="admin-card-group">
              <div className="admin-card-b">
                <span className="card-name-b">Highest All-time Sales</span>
                <div className="card-oval">
                  <img src='' alt='' />
                </div>
                <span className="card-name-b">Patience Agbor</span>
                <span className="card-name-b litt">34 Sales</span>
              </div>
              <div className="admin-card-b">
                <span className="card-name-b">Highest All-time Sales</span>
                <div className="card-oval">
                  <img src='' alt=''/>
                </div>
                <span className="card-name-b">Patience Agbor</span>
                <span className="card-name-b litt">34 Sales</span>
              </div>
              <div className="admin-card-b">
                <span className="card-name-b">Highest All-time Sales</span>
                <div className="card-oval">
                  <img src='' alt='' />
                </div>
                <span className="card-name-b">Patience Agbor</span>
                <span className="card-name-b litt">34 Sales</span>
              </div>
            </div>
          </div> */}
        </div>
      </div>
    </div>
  );
}
