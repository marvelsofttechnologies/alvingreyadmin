import React, { useContext, useState } from "react";
import { Formik, FormikHelpers, Form, Field } from "formik";
import { LoginInput } from "../../utilities/Interfaces";
import Spinner from "../../utilities/Spinner";
import { User } from "../../classes/User";
import { MainContext } from "../../contexts/MainContext";
import { useHistory } from "react-router";

export default function SignIn() {
  const history = useHistory();
  const initialValues: LoginInput = { email: "", password: "" };
  const [loading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const { setUser } = useContext(MainContext);

  const login = async (details: LoginInput) => {
    setLoading(true);
    let user = new User();
    let data = await user.Login(details);
    if (data?.status) {
      setUser(user._userView ?? {});
      localStorage.setItem("user", JSON.stringify(user._userView ?? {}));
      localStorage.setItem("token", user._userView?.token ?? "");
      history.push("/dashboard");
      return;
    }
    console.log(user._userView);
    setErrorMessage(data?.message ?? "");
    setLoading(false);
  };

  return (
    <>
      <div>
        <div className="login-body">
          <div className="ag-login">
            <div className="container-fluid">
              <div className="row">
                <div className="col-lg-6">
                  <h4 className="ag-login-text">Welcome Back, Sign In</h4>
                  <div className="login-image">
                    <img src="/assets/signin-icon.png" alt="" />
                  </div>
                </div>
                <div className="col-lg-5">
                  <Formik
                    initialValues={initialValues}
                    onSubmit={async (
                      values: LoginInput,
                      { setSubmitting }: FormikHelpers<LoginInput>
                    ) => {
                      await login(values);
                      //   setTimeout(() => {
                      //     alert(JSON.stringify(values, null, 2));
                      //     setSubmitting(true);
                      //   }, 500);
                    }}
                  >
                    <Form className="login-form">
                      <p className="text-danger">{errorMessage}</p>
                      <div className="formbox">
                        <label className="ag-form-label">Email</label>
                        <Field
                          id="email"
                          name="email"
                          className="ag-formfield"
                          placeholder=""
                        />
                      </div>
                      <div className="formbox">
                        <label className="ag-form-label">Password</label>
                        <Field
                          type="password"
                          name="password"
                          className="ag-formfield"
                          placeholder="* * * * * * * * *"
                          required
                        />
                      </div>
                      <button className="ag-btn red mt-5 w-100" type="submit">
                        {loading ? <Spinner /> : "Submit"}
                      </button>
                    </Form>
                  </Formik>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
