export interface IUser {
    firstName: string
    lastName: string
    email: string
} 

export interface IRoute {
    exact: boolean
    component: | React.ComponentType<React.ComponentProps<any>> | React.ComponentType<any>
    path: string
    key: string
    routes?: IRoute[]
}

export interface ITheme {
    primary: string,
    secondary:string
}

export interface UserInput {
    email?:         string;
    password?:      string;
    firstName?:     string;
    lastName?:      string;
    phoneNumber?:   string;
    usersName?:     string;
    country?:       string;
    accountNumber?: string;
    accountName?:   string;
    bankName?:      string;
    reffererCode?:  string;
    media?:         MediaInput[];
    socialMedia?:      SocialMedia;
    description?:   string;
    officeAddress?: string;

}

export interface MediaInput {
    name?:         string;
    extention?:    string;
    base64String?: string;
    propertyId?:   number;
    isImage?:      boolean;
    isVideo?:      boolean;
    isDocument?:   boolean;
}

export interface UserView {
    id?:                number;
    firstName?:         string;
    lastName?:          string;
    token?:             string;
    phoneNumber?:       string;
    refferalCode?:      string;
    fullName?:          string;
    downlines?:         UserView[];
    numberOfDownlines?: number;
    medias?:            MediaView[];
    dateCreated?:       Date;
    usersName?:         string;
    upline?:            UserView;
    email?:             string;
    socialMedia?:      SocialMedia;
    address?:   string;
    bankName?: string;
    accountName?: string;
    accountNumber?: string;
    description?:   string;
    officeAddress?: string;
    rating?: number;
    yearsOfExperience?: number;
}

export interface MediaView {
    id?: number;
    url?:        string;
    isImage?:    boolean;
    isVideo?:    boolean;
    isDocument?: boolean;
}

export interface LoginInput {
    email?:    string;
    password?: string;
}

export interface Response {
    status: boolean;
    message: string;
    data: any;
}

export interface ApiResponse {
    status: any;
    message: string;
    data: any;
    error: any[];
    relations:any;
    href: string;
    method: string;
    routeName: string;
    routeValues: any;
}

export interface ApiLink{
    href: string;
    rel: string[];
}

export interface PagedApiResponse extends ApiResponse {
    data: PagedApiResponseData;
}

export interface PagedApiResponseData {
    offset?: string;
    limit?: string;
    size?: number;
    value?: any[]; 
    first?: ApiLink;
    last?: ApiLink;
    next?:  ApiLink;
    self?:   ApiLink;
}

export interface ContextData {
    user?: UserView;
}

export interface SocialMedia {
    whatsapp?:  string;
    facebook?:  string;
    twitter?:   string;
    youtube?:   string;
    instagram?: string;
    linkedIn?:  string;
}

export interface UserVerificationInput {
    code: string;
}

export interface PropertyView {
    id?:                number;
    name?:              string;
    title?:             string;
    address?:           string;
    description?:       string;
    sellMyself?:        boolean;
    price?:             number;
    numberOfBedrooms?:  number;
    numberOfBathrooms?: number;
    isDraft?:           boolean;
    isActive?:          boolean;
    isForRent?:         boolean;
    isForSale?:         boolean;
    area?:              string;
    propertyType?:      string;
    createdByUser?:     string;
    mediaFiles?:        MediaView[];
    verified?:          boolean;
    representative?:    UserView;
    state?:             string;
    lga?:               string;
    isRequest?:         boolean;
    propertyTypeId?:    number;
    status?: string;
}

export interface Media {
    url?:        string;
    isImage?:    boolean;
    isVideo?:    boolean;
    isDocument?: boolean;
}

export interface PropertyInput {
    id?: number;
    name?:              string;
    title?:             string;
    address?:           string;
    description?:       string;
    sellMyself?:        boolean;
    price?:             number;
    numberOfBedrooms?:  number;
    numberOfBathrooms?: number;
    isDraft?:           boolean;
    isActive?:          boolean;
    isForRent?:         boolean;
    isForSale?:         boolean;
    propertyTypeId?:    number;
    mediaFiles?:        MediaInput[];
    state?:             string;
    lga?:               string;
    area?:              string;
    isRequest?:         boolean;
    comment?:           string;
    budget?:            string;
    statusId?: number;
}

export interface MediaInput {
    name?:         string;
    extention?:    string;
    base64String?: string;
    propertyId?:   number;
    isImage?:      boolean;
    isVideo?:      boolean;
    isDocument?:   boolean;
}

export interface RatingInput {
    userId?: number;
    rating?: number;
}