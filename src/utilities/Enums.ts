export const Statuses = {
    PENDING: "PENDING",
    APPROVED: "APPROVED",
    ONGOING: "ONGOING",
    RESOLVED: "RESOLVED",
    VERIFIED: "VERIFIED",
    DRAFTED: "DRAFTED",
    ONSALE:"ONSALE",
    SOLD: "SOLD",
    DRAFT:"DRAFT",
    TRUE:true, 
    FALSE:false
}

export const HttpMethods ={
    POST: "POST",
    GET: "GET",
    PUT: "PUT",
    DELETE: "DELETE",
}

export const ErrorMessages ={
    FAILED: "An Error occurred, Please try again later"
}

export const EndPoints ={
    User: {
        LOGIN:"admin/token",
        REGISTER: "user/register",
        VERIFY:"user/verifyuser/",
        UPDATE: "user/update",
        LIST:"user/list",
        DELETE:"user/delete",
        COUNT:{ 
            USERS:'user/count',
            DOWNLINES:'user/downlines/count'
        },

    },
    Property:{
        LIST:"property/list",
        GET:"property/get/", 
        CREATE:"property/create",
        UPDATE:"property/update",
        DELETE:"property/delete/"
    },
    Admin:{
        UPDATEPROPERTY:'admin/property/update',
        CREATE:"admin/create",
        LIST: "admin/list",
        RATEUSER:"admin/rate",
        GENDERMETRICS:"admin/gender/metrics",
    },
    Media:{
        DELETE:"media/delete/",
    },
}


