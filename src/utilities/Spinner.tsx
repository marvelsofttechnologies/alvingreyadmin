import React from "react";
import { RotateSpinner } from "react-spinners-kit";

function Spinner(props: any) {
	const { size, color } = props;
	return (
		<RotateSpinner
			size={!size ? 20 : size}
			color={!color ? "#fff" : color == "primary" ? "#cd1a1d" : color}
			loading={true}
		/>
	);
}

export default Spinner;
