import { EndPoints, ErrorMessages, HttpMethods, Statuses } from "../utilities/Enums";
import Fetch from "../utilities/Fetch";
import {  ApiResponse, PagedApiResponse, PropertyInput, PropertyView, Response } from "../utilities/Interfaces";

export default  class Property{
    _propertyView?: PropertyView
    _propertyInput?: PropertyInput

    constructor(public id?: number){}

    public static List = async (url?:any) => {
        let response: Response;
        try {
            let data = await Fetch(`${EndPoints.Property.LIST}${url ? url : ''}`) as unknown as PagedApiResponse;
            response = {
                status: data.status,
                message: data.message,
                data: data.data,
              };
              return response;
        } catch (error) {
            console.error(error);
            
        }
    }

    public static GetById = async(id: number)=> {
        let response: Response = {
            status: Statuses.FALSE,
            message: ErrorMessages.FAILED,
            data: null,
          };
          try {
              let data = await Fetch(`${EndPoints.Property.GET}${id}`) as unknown as ApiResponse;
              response = {
                status: data.status,
                message: data.message,
                data: data.data,
              };
              return response;
          } catch (error) {
              console.log(error);
              return response;
          }
    }

    public static Add = async (property: PropertyInput) => {
        let response: Response = {
            status: Statuses.FALSE,
      message: ErrorMessages.FAILED,
      data: null,
        };
        try{
            let data = await Fetch(EndPoints.Property.CREATE,HttpMethods.POST,property) as unknown as ApiResponse;
            response = {
                status: data.status,
                message: data.message,
                data: data.data,
              };
              return response;
        }catch(error){
            console.log(error);
            return response;
        }
    }

    public static GetTypes = async () => {

    }

    public static Update = async (property: PropertyInput) =>{
        let response: Response = {
            status: Statuses.FALSE,
      message: ErrorMessages.FAILED,
      data: null,
        }
        try {
            let data = await Fetch(EndPoints.Admin.UPDATEPROPERTY,HttpMethods.POST,property) as unknown as ApiResponse;
            response = {
                status: data.status,
                message: data.message,
                data: data.data,
              };
              return response;
        } catch (error) {
            console.error(error);
            return response;
            
        }
    }

    public static DeleteMedia = async (id:any) =>{
        let response: Response = {
            status: Statuses.FALSE,
            message: ErrorMessages.FAILED,
            data: null,
          };
          try {
              let data = await Fetch(`${EndPoints.Media.DELETE}${id}`) as unknown as ApiResponse;
              response = {
                status: data.status,
                message: data.message,
                data: data.data,
              };
              return response;
          } catch (error) {
              console.log(error);
              return response;
          }
    }

    public static Delete = async(id?: number)=> {
        let response: Response = {
            status: Statuses.FALSE,
            message: ErrorMessages.FAILED,
            data: null,
          };
          try {
              let data = await Fetch(`${EndPoints.Property.DELETE}${id}`) as unknown as ApiResponse;
              response = {
                status: data.status,
                message: data.message,
                data: data.data,
              };
              return response;
          } catch (error) {
              console.log(error);
              return response;
          }
    }
}