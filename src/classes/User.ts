import {
  EndPoints,
  ErrorMessages,
  HttpMethods,
  Statuses,
} from "../utilities/Enums";
import Fetch from "../utilities/Fetch";
import {
  LoginInput,
  Response,
  ApiResponse,
  UserView,
  UserInput,
  UserVerificationInput,
  PagedApiResponse,
  RatingInput,
} from "../utilities/Interfaces";

export class User {
  _userView?: UserView;
  _userInput?: UserInput;

  constructor(public email?: string, public id?: number) {}

  public Login = async (details: LoginInput) => {
    let response: Response;
    try {
      let data = (await Fetch(
        EndPoints.User.LOGIN,
        HttpMethods.POST,
        details
      )) as unknown as ApiResponse;
      response = {
        status: data.status,
        message: data.message,
        data: data.data,
      };
      if (data.status) {
        this._userView = data.data as UserView;
      }
      return response;
    } catch (error) {
      console.log(error);
    }
  };

  public Register = async (details: UserInput) => {
    let response: Response = {
      status: Statuses.FALSE,
      message: ErrorMessages.FAILED,
      data: null,
    };
    try {
      let data = (await Fetch(
        EndPoints.User.REGISTER,
        HttpMethods.POST,
        details
      )) as unknown as ApiResponse;
      response = {
        status: data.status,
        message: data.message,
        data: data.data,
      };
      return response;
    } catch (error) {
      console.error(error);
      return response;
    }
  };

  public verify = async (details: UserVerificationInput) => {
    let response: Response = {
        status: Statuses.FALSE,
        message: ErrorMessages.FAILED,
        data: null,
      };
      try {
          let data = await Fetch(`${EndPoints.User.VERIFY}/${details.code}`) as unknown as ApiResponse;
          if(!data.status){
              response.message = data.message;
              return response;
          }

          response.status = data.status;
          response.message = data.message;
          response.data = data.data;
          return response;

      } catch (error) {
          
      }
  };

  public static list = async (url?:any) => {
    let response: Response = {
      status: Statuses.FALSE,
      message: ErrorMessages.FAILED,
      data: null,
    };
    try{
      let data = await Fetch(`${EndPoints.User.LIST}${url ? url : ''}`) as unknown as PagedApiResponse;
      response = {
        status: data.status,
        message: data.message,
        data: data.data,
      };
      return response;
    }catch(error){
      console.log(error);
      return response;
    }
  };

  public static listAdmin = async (url?:any) => {
    let response: Response = {
      status: Statuses.FALSE,
      message: ErrorMessages.FAILED,
      data: null,
    };
    try{
      let data = await Fetch(`${EndPoints.Admin.LIST}${url ? url : ''}`) as unknown as PagedApiResponse;
      response = {
        status: data.status,
        message: data.message,
        data: data.data,
      };
      return response;
    }catch(error){
      console.log(error);
      return response;
    }
  };

  public static count = async () => {
    let response: Response = {
      status: Statuses.FALSE,
      message: ErrorMessages.FAILED,
      data: null,
    };
    try {
      let data = await Fetch(`${EndPoints.User.COUNT.USERS}`) as unknown as ApiResponse;
      response = {
        status: data.status,
        message: data.message,
        data: data.data,
      };
      return response;
    } catch (error) {
      console.log(error);
      return response;
    }
  }

  public static downlinesCount = async () => {
    let response: Response = {
      status: Statuses.FALSE,
      message: ErrorMessages.FAILED,
      data: null,
    };
    try {
      let data = await Fetch(`${EndPoints.User.COUNT.DOWNLINES}`) as unknown as ApiResponse;
      response = {
        status: data.status,
        message: data.message,
        data: data.data,
      };
      return response;
    } catch (error) {
      console.log(error);
      return response;
    }
  }

  public static Update =async (details: UserInput) => {
    let response: Response = {
        status: Statuses.FALSE,
        message: ErrorMessages.FAILED,
        data: null,
      };
      try{
          let data = await Fetch(`${EndPoints.User.UPDATE}`,HttpMethods.POST,details) as unknown as ApiResponse;
          response.status = data.status;
          response.message = data.message;
          response.data = data.data;
          return response;
      }catch (error) {
          console.error(error);
          return response;
      }

    }

    public static  createAdmin = async (details: UserInput) => {
      let response: Response = {
        status: Statuses.FALSE,
        message: ErrorMessages.FAILED,
        data: null,
      };
      try{
          let data = await Fetch(`${EndPoints.Admin.CREATE}`,HttpMethods.POST,details) as unknown as ApiResponse;
          response.status = data.status;
          response.message = data.message;
          response.data = data.data;
          return response;
      }catch (error) {
          console.error(error);
          return response;
      }

    }

    public static rateUser = async (details: RatingInput) => {
      let response: Response = {
        status: Statuses.FALSE,
        message: ErrorMessages.FAILED,
        data: null,
      };
      try{
          let data = await Fetch(`${EndPoints.Admin.RATEUSER}`,HttpMethods.POST,details) as unknown as ApiResponse;
          response.status = data.status;
          response.message = data.message;
          response.data = data.data;
          return response;
      }catch (error) {
          console.error(error);
          return response;
      }
    }

    public static delete = async (email?: string) => {
      let response: Response = {
        status: Statuses.FALSE,
        message: ErrorMessages.FAILED,
        data: null,
      };
      try{
          let data = await Fetch(`${EndPoints.User.DELETE}/${email}`,HttpMethods.GET) as unknown as ApiResponse;
          response.status = data.status;
          response.message = data.message;
          response.data = data.data;
          return response;
      }catch (error) {
          console.error(error);
          return response;
      }
    }

    public static initiatePasswordReset = async (email?: string) => {
      let response: Response = {
        status: Statuses.FALSE,
        message: ErrorMessages.FAILED,
        data: null,
      };
      try{
          let data = await Fetch(`${EndPoints.User.DELETE}/${email}`,HttpMethods.GET) as unknown as ApiResponse;
          response.status = data.status;
          response.message = data.message;
          response.data = data.data;
          return response;
      }catch (error) {
          console.error(error);
          return response;
      }
    }
    
    public static genderMetrics = async () => {
      let response: Response = {
        status: Statuses.FALSE,
        message: ErrorMessages.FAILED,
        data: null,
      };
      try {
        let data = await Fetch(`${EndPoints.Admin.GENDERMETRICS}`) as unknown as ApiResponse;
        response = {
          status: data.status,
          message: data.message,
          data: data.data,
        };
        return response;
      } catch (error) {
        console.log(error);
        return response;
      }
    }

}
