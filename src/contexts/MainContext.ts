import React from "react";
import { User } from '../classes/User'
import { ContextData, UserView } from "../utilities/Interfaces";
const data: ContextData = {
};

export const MainContext = React.createContext({
	data,
	setUser: (user: UserView) => {},
});
