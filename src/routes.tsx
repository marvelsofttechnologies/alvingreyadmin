import React from "react";
import { Switch, Route } from "react-router-dom";
import SignIn from "./pages/authentication/SignIn";
import Dashboard from "./pages/Dashboard/Dashboard";
import Properties from "./pages/Properties/Properties";
import PropertyDetails from "./pages/Properties/PropertyDetails";
import Admins from "./pages/User/Admins";
import Realtors from "./pages/User/Realtors";

import { IRoute } from "./utilities/Interfaces";

const ROUTES: IRoute[] = [
  { path: "/", key: "ROOT", exact: true, component: () => <SignIn /> },
  { path: "/dashboard", key: "ROOT", exact: true, component: () => <Dashboard /> },
  { path: "/realtors", key: "ROOT", exact: true, component: () => <Realtors /> },
  { path: "/properties", key: "ROOT", exact: true, component: () => <Properties /> },
  { path: "/properties/:id", key: "ROOT", exact: true, component: () => <PropertyDetails /> },
  { path: "/admin", key: "ROOT", exact: true, component: () => <Admins /> },
];
export default ROUTES;

function RouteWithSubRoutes(route: IRoute) {
  return (
    <Route
      path={route.path}
      exact={route.exact}
      render={(props) => <route.component {...props} routes={route.routes} />}
    />
  );
}
export function RenderRoutes({ routes }: { routes: IRoute[] }) {
  return (
    <>
      <Switch>
        {routes.map((route, i) => {
          return <RouteWithSubRoutes {...route} />;
        })}
        <Route component={() => <h1>Page not available!</h1>} />
      </Switch>
    </>
  );
}
