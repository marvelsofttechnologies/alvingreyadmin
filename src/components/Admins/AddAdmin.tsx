import React, { useState } from "react";
import { UserInput } from "../../utilities/Interfaces";
import { Formik, FormikHelpers, Form, Field } from "formik";
import Spinner from "../../utilities/Spinner";
import { User } from "../../classes/User";
import { toast } from "react-toastify";

export default function AddAdmin({ close }: { close: any }) {
  const [loading, setLoading] = useState(false);
  const initialValues: UserInput = {
    firstName: "",
    lastName: "",
    email: "",
  };

  const create = async (details:UserInput) => {
    setLoading(true);
    let data = await User.createAdmin(details);
    if(data.status){
        toast.success(data.message);
        setLoading(false);
        close();
        return;
    }
    toast.error(data.message);
    setLoading(false);
    return;

  }
  return (
    <div>
      <div className="flex-item mb-4 w-100">
        <h5 className="modal-title">Add New Admin</h5>
        <i className="fas fa-times mod" onClick={() => close()} />
      </div>
      <Formik
        initialValues={initialValues}
        onSubmit={async (
          values: UserInput,
          { setSubmitting }: FormikHelpers<UserInput>
        ) => {
          console.log(values);
          await create(values);
        }}
      >
        <Form>
          <div className="row">
            <div className="col-lg">
              <div className="single-form">
                <label className="admin-form">First Name</label>
                <Field
                  id="firstName"
                  name="firstName"
                  className="admin-form-field"
                />
              </div>
              <div className="single-form">
                <label className="admin-form">Last Name</label>
                <Field
                  id="lastName"
                  name="lastName"
                  className="admin-form-field"
                />
              </div>
              <div className="single-form">
                <label className="admin-form">Email</label>
                <Field id="email" name="email" className="admin-form-field" />
              </div>
              <button className="admin-update w-100 mt-5" type="submit">{loading ? <Spinner/> : " Add New Admin" }</button>
            </div>
          </div>
        </Form>
      </Formik>
    </div>
  );
}
