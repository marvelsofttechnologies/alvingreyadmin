import React from "react";
import { UserView } from "../../utilities/Interfaces";

export default function ProfileTabAdmin({admin}: {admin:UserView}) {
  return (
    <div className="profile-tab-content">
      <div className="row">
        <div className="col-lg-3">
          <div className="per-details">
            <label className="tab-label">First Name</label>
            <span className="tab-text">{admin.firstName}</span>
          </div>
          <div className="per-details">
            <label className="tab-label">Last Name</label>
            <span className="tab-text">{admin.lastName}</span>
          </div>
          <div className="per-details">
            <label className="tab-label">Email Address</label>
            <span className="tab-text">{admin.email}</span>
          </div>
        </div>
      </div>
    </div>
  );
}
