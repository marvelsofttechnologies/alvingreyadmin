import React from "react";
import { UserView } from "../../utilities/Interfaces";

export default function PermissionsTab({admin}: {admin:UserView}) {
  return (
    <div className="network-tab-content mt-3">
      <div className="row">
        <div className="col-lg-6">
          <div className="admin-action">
            <span className="permision">View Analytics</span>
            <div className="switch mb-0">
              <label className="ag-toggle">
                <input type="checkbox" defaultChecked />
                <span className="slider" />
              </label>
            </div>
          </div>
          <div className="admin-action wh">
            <span className="permision">View Realtor Details</span>
            <div className="switch mb-0">
              <label className="ag-toggle">
                <input type="checkbox" defaultChecked/>
                <span className="slider" />
              </label>
            </div>
          </div>
          <div className="admin-action">
            <span className="permision">View Ratings</span>
            <div className="switch mb-0">
              <label className="ag-toggle">
                <input type="checkbox"defaultChecked />
                <span className="slider" />
              </label>
            </div>
          </div>
          <div className="admin-action wh">
            <span className="permision">View Admin Users</span>
            <div className="switch mb-0">
              <label className="ag-toggle">
                <input type="checkbox"defaultChecked />
                <span className="slider" />
              </label>
            </div>
          </div>
          <div className="admin-action">
            <span className="permision">Modify Analytics</span>
            <div className="switch mb-0">
              <label className="ag-toggle">
                <input type="checkbox" defaultChecked/>
                <span className="slider" />
              </label>
            </div>
          </div>
          <div className="admin-action wh">
            <span className="permision">Modify User Details</span>
            <div className="switch mb-0">
              <label className="ag-toggle">
                <input type="checkbox" defaultChecked/>
                <span className="slider" />
              </label>
            </div>
          </div>
          <div className="admin-action">
            <span className="permision">Modify Realtor Ratings</span>
            <div className="switch mb-0">
              <label className="ag-toggle">
                <input type="checkbox" defaultChecked/>
                <span className="slider" />
              </label>
            </div>
          </div>
          <div className="admin-action wh">
            <span className="permision">Modify Backend Admin</span>
            <div className="switch mb-0">
              <label className="ag-toggle">
                <input type="checkbox" defaultChecked/>
                <span className="slider" />
              </label>
            </div>
          </div>
          <div className="admin-action">
            <span className="permision">View Properties</span>
            <div className="switch mb-0">
              <label className="ag-toggle">
                <input type="checkbox" defaultChecked/>
                <span className="slider" />
              </label>
            </div>
          </div>
          <div className="admin-action wh">
            <span className="permision">Modify Properties</span>
            <div className="switch mb-0">
              <label className="ag-toggle">
                <input type="checkbox" defaultChecked />
                <span className="slider" />
              </label>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
