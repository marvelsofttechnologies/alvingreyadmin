import React, { useState } from "react";
import { useLocation } from "react-router-dom";
import { SideNav } from "../Navigations/SideNav";
import TopNav from "../Navigations/TopNav";

export default function Template(props: any) {
  const location = useLocation();
  const [navOpen,setNavOpen] = useState(true);

  const toggleSideNav = () => {
      setNavOpen(!navOpen);
  }

  return (
    <>
      {location.pathname === "/" ? (
          <>
        {props.children}
        </>
      ) : (
        <>
          <div className={navOpen ? "admin-body show" : "admin-body"}>
            <SideNav toggleSideNav={toggleSideNav}/>
            <div className="body-content">
              <TopNav />
              {props.children}
            </div>
          </div>
        </>
      )}
    </>
  );
}
