import React from 'react'
import {
    EmailShareButton,
    FacebookShareButton,
    HatenaShareButton,
    InstapaperShareButton,
    LineShareButton,
    LinkedinShareButton,
    LivejournalShareButton,
    MailruShareButton,
    OKShareButton,
    PinterestShareButton,
    PocketShareButton,
    RedditShareButton,
    TelegramShareButton,
    TumblrShareButton,
    TwitterShareButton,
    ViberShareButton,
    VKShareButton,
    WhatsappShareButton,
    WorkplaceShareButton
  } from "react-share";
  import {
    EmailIcon,
    FacebookIcon,
    FacebookMessengerIcon,
    HatenaIcon,
    InstapaperIcon,
    LineIcon,
    LinkedinIcon,
    LivejournalIcon,
    MailruIcon,
    OKIcon,
    PinterestIcon,
    PocketIcon,
    RedditIcon,
    TelegramIcon,
    TumblrIcon,
    TwitterIcon,
    ViberIcon,
    VKIcon,
    WeiboIcon,
    WhatsappIcon,
    WorkplaceIcon
  } from "react-share";
import { toast } from 'react-toastify';

export default function ShareProfile(url:any) {
    console.log(url);
    const copyToClip = async (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
        e.preventDefault();
        const copied = await navigator.clipboard.writeText(url.url);
        console.log(copied);
        toast.success("Link copied successfully");
      };
    return (
        <div className="row">
            <div className="col">
                <FacebookShareButton url={url.url} >
                    <FacebookIcon round></FacebookIcon>
                </FacebookShareButton>
                <TwitterShareButton url={url.url} >
                    <TwitterIcon round></TwitterIcon>
                </TwitterShareButton>
                <LinkedinShareButton url={url.url} >
                    <LinkedinIcon round></LinkedinIcon>
                </LinkedinShareButton>
                <TelegramShareButton url={url.url} >
                    <TelegramIcon round></TelegramIcon>
                </TelegramShareButton>
                <WhatsappShareButton url={url.url} >
                    <WhatsappIcon round></WhatsappIcon>
                </WhatsappShareButton>
                <button type="button" className="btn btn-light btn-circle btn-xl"><i className='fas fa-copy' onClick={async(e) =>await copyToClip(e)}></i></button>
            </div>
        </div>
    )
}
