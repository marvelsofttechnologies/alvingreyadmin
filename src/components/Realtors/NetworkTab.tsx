import React, { useState } from "react";
import { toast } from "react-toastify";
import { UserView } from "../../utilities/Interfaces";

export default function NetworkTab({realtor}: {realtor?:UserView}) {
    const [link, setLink] = useState(`www.alvingrey.netlify.app/account/register/${realtor?.refferalCode}`);

    const copyToClip = async (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
        e.preventDefault();
        const copied = await navigator.clipboard.writeText(link);
        console.log(copied);
        toast.success("Link copied successfully");
      };



  return (
    <div className="network-tab-content">
      <div className="row">
        <div className="col-lg-3">
          <div className="per-content">
            <h5 className="network-tab-title">Total Downlines</h5>
            <h2 className="network-count">{realtor?.downlines?.length ?? 0}</h2>
          </div>
          <div className="per-content">
            <h5 className="network-tab-title">Upline</h5>
            <div className="user-box-l">{realtor?.upline?.fullName}</div>
          </div>
        </div>
        <div className="col-lg-3">
          <div className="per-content">
            <h5 className="network-tab-title">Downlines</h5>
            {realtor?.downlines?.map((downline,index) => {
                return <div className="user-box-l">{downline.fullName}</div>
            })}
          </div>
        </div>
        <div className="col-lg-4">
          <div className="per-content">
            <h5 className="network-tab-title">My Referral code</h5>
            <div className="user-btn flex-item">
              {link}
              <i className="far fa-copy mr-0" onClick={(e) => copyToClip(e)}/>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
