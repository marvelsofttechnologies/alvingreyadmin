import React, { useState } from "react";
import Rating from "react-rating";
import { User } from "../../classes/User";
import { UserView } from "../../utilities/Interfaces";

export default function ProfileTab({realtor}: {realtor?: UserView}) {
    const [rate,setRate] = useState(realtor?.rating);

    const rateUser = async (rate: number) => {
        try {
            let data = await User.rateUser({userId: realtor?.id,rating:rate});
            console.log(data);
            setRate(rate);
            window.location.reload();
            
        } catch (error) {
            console.log(error);
        }
    }
  return (
    <div className="profile-tab-content ">
      <div className="row">
        <div className="col-lg-3">
          <div className="per-details">
            <label className="tab-label">First Name</label>
            <span className="tab-text">{realtor?.firstName}</span>
          </div>
          <div className="per-details">
            <label className="tab-label">Last Name</label>
            <span className="tab-text">{realtor?.lastName}</span>
          </div>
          <div className="per-details">
            <label className="tab-label">Email Address</label>
            <span className="tab-text">{realtor?.email}</span>
          </div>
          <div className="per-details">
            <label className="tab-label">Phone Number</label>
            <span className="tab-text">{realtor?.phoneNumber}</span>
          </div>
          <div className="per-details">
            <label className="tab-label">Address</label>
            <span className="tab-text">{realtor?.address}</span>
          </div>
          <div className="per-details">
            <label className="tab-label">Years of Experience</label>
            <span className="tab-text">{realtor?.yearsOfExperience}</span>
          </div>
        </div>
        <div className="col-lg-4">
          <div className="switch">
            <span className="checkbox-text">Account Active</span>
            <label className="ag-toggle">
              <input type="checkbox" defaultChecked />
              <span className="slider" />
            </label>
          </div>
          <div className="admin-rating">
            <div className="rate-text">Rate this realtor</div>
            <Rating initialRating={realtor?.rating} emptySymbol={<i className="fas fa-star rating-size" />} fullSymbol={<i className="fas fa-star text-danger rating-size"/>} onClick={async (rate) => {await rateUser(rate);}} fractions={2}/>
          </div>
        </div>
      </div>
    </div>
  );
}
