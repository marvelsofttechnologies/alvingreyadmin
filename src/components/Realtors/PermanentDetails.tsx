import React, { useState } from "react";
import { toast } from "react-toastify";
import { User } from "../../classes/User";
import { UserView } from "../../utilities/Interfaces";
import Modal from "../../utilities/Modal";
import ShareProfile from "../Generics/ShareProfile";

export default function PermanentDetails({
  realtor,
  list,
}: {
  realtor?: UserView;
  list?: any;
}) {
  console.log(realtor);
  const [shareModalOpen, setShareModalOpen] = useState(false);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);

  const deleteUser = async (email?: string) => {
    try {
      let data = await User.delete(email);
      if (data.status) {
        toast.success("User deleted Successfully");
        await list();
        return;
      }
      toast.error("An error occurred");
    } catch (error) {
      console.error(error);
      toast.error("An error occurred");
    }
  };
  return (
    <>
      <Modal
        open={shareModalOpen}
        onClose={() => {
          setShareModalOpen(false);
        }}
        height={"25%"}
        width={"50%"}
      >
        <ShareProfile
          url={`https://alvingrey.netlify.app/profile/shared/${realtor?.id}`}
        />
      </Modal>
      <Modal
        open={deleteModalOpen}
        onClose={() => {
          setShareModalOpen(false);
        }}
        height={'150px'}
        width={'30%'}
      >
        <div className="flex-column justify-content-center">
          <h5 >
            Are you sure you want to delete <span>{realtor?.fullName} </span>
          </h5>
          <div className="row  flex-center justify-space-between mt-3">
            <div className="col">
            <button
              className="user-red-btn-a "
              style={{ backgroundColor: "#000"}}
              onClick={async () => await setDeleteModalOpen(false)}
            >
              No
            </button>
            </div>
            <div className='col'>

          <button
            className="user-red-btn-a "
            onClick={async () => await deleteUser(realtor?.email)}
          >
            Yes
          </button>
            </div>
          </div>
        </div>
      </Modal>

      <div className="flex-item divided">
        <div className="flex-unrestricted divided">
          <div className="user-avatar">
            <img
              src={
                realtor?.medias && realtor?.medias.length > 0
                  ? realtor.medias[0].url
                  : "/assets/Alvino.svg"
              }
              alt=""
            />
          </div>
          <div className="username">
            <h5 className="user-name-a">{realtor?.fullName}</h5>
          </div>
          <div className="user-loc-a">
            <div className="ag-role">Alvin Grey Associate</div>
            <div className="ag-location">{realtor?.address}</div>
          </div>
          <div className="user-ratings">
            <span className="rate">{realtor?.rating}</span>
            <div className="tins">Rating</div>
          </div>
        </div>
        <button
          className="user-red-btn-a"
          style={{ backgroundColor: "#000" }}
          onClick={async () => await setDeleteModalOpen(true)}
        >
          <i className="fas fa-trash" />
          Delete
        </button>
        <button
          className="user-red-btn-a"
          onClick={() => setShareModalOpen(true)}
        >
          <i className="fas fa-share" />
          Share this profile
        </button>
      </div>
    </>
  );
}
