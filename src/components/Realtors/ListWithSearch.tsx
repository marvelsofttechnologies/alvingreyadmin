import React, { useState } from "react";
import { UserView } from "../../utilities/Interfaces";

export default function ListWithSearch({realtors,totalPages,setUser,paginate,currentPage,search,}:{realtors: UserView[],totalPages?: number,setUser:any,paginate?:any,currentPage?:number,search?:any,} ) {


  return (
    <div className="list-users">
      <div className="admin-search-bar">
        <i className="far fa-search" />
        <input type="text" className="admin-search" placeholder="Search" onChange={(e) => search(e.target.value)}/>
      </div>
      {
          realtors.map((realtor,index) => {
              return <div className="user-box" onClick={() => setUser(realtor.id)}>{realtor.fullName}</div>
          })
      }
      <div className="pagination">
        <div className="page-arrows" onClick={() => {
              paginate('previous');
          }
        }>
          <i className="fal fa-angle-left" onClick={() => {
              paginate('previous');
          }
        } />
        </div>
        <div className="page-number">{currentPage}</div>
        <div className="page-number nb">{`of`}</div>
        <div className="page-number">{`${totalPages}`}</div>
        <div className="page-arrows" onClick={() =>{ 
              paginate('next');
              }}>
          <i className="fal fa-angle-right" onClick={() =>{ 
              paginate('next');
              }}/>
        </div>
      </div>
    </div>
  );
}
