import { useCallback, useEffect, useState } from 'react';
import { MainContext } from '../../contexts/MainContext';
import ROUTES, { RenderRoutes } from '../../routes';
import './App.css';
import { UserView } from '../../utilities/Interfaces';
import Template from '../Generics/Template';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';




function App() {

  const [data,setData] = useState({});

  
  const setUser = useCallback( (user?: UserView) => {
    localStorage.setItem('user', JSON.stringify(user));
    setData({...data,user:user})
  },[data,setData]);

  useEffect(() => {
    localStorage.getItem('user') ? setUser(JSON.parse(localStorage.getItem('user') || '{}')) : localStorage.setItem('','')
  },[])

  return (
    <MainContext.Provider value={{data,setUser}}>

      <Template>
      <ToastContainer />
        <RenderRoutes routes={ROUTES} />
      </Template>
    </MainContext.Provider>
  );
}

export default App;
