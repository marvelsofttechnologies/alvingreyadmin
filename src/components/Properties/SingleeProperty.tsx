import React from "react";
import { PropertyView } from "../../utilities/Interfaces";
import parse from "html-react-parser";
import { SRLWrapper } from "simple-react-lightbox";
import Naira from 'react-naira' ;

export default function SingleeProperty({
  property,
}: {
  property?: PropertyView;
}) {
  return (
    <div className="col-lg-4">
      <button
        className="ag-btn bordered re w-100"
        onClick={() => window.location.reload()}
      >
        Reload Properties
      </button>
      <div className="property-info-box">
        <div className="property-img">
          <img
            src={
              property?.mediaFiles && property?.mediaFiles.length >= 1
                ? property.mediaFiles.find((r) => r.isImage)?.url
                : "https://upload.wikimedia.org/wikipedia/commons/3/3f/Placeholder_view_vector.svg"
            }
            alt=""
          />
        </div>
        <div className="property-details-box">
          <h6 className="property-box-title">{property?.name}</h6>
          <div className="flex-item">
            <p className="property-box-sub">
              <i className="fal fa-bed" />{" "}
              {`${property?.numberOfBedrooms} ${property?.numberOfBedrooms  && property.numberOfBedrooms > 1 ? "Bedrooms" : "bedroom"}`}
            </p>
            <p className="property-box-sub">
              <i className="fal fa-shower" />{" "}
              {`${property?.numberOfBathrooms} ${property?.numberOfBathrooms  && property.numberOfBathrooms > 1 ? "Bathrooms" : "Bathroom"}`}
            </p>
            <p className="property-box-sub">
              <i className="fal fa-tape" /> 4 Bedrooms
            </p>
          </div>
          <div className="flex-item">
            <h2 className="property-price"><Naira>{property?.price}</Naira></h2>
            {/* <p className="property-box-sub b">
              <i className="fal fa-tape" /> {property?.title}
            </p> */}
          </div>
          <h6 className="desc-title">Description</h6>
          <ul className="desc">
            {parse(property?.description ? property.description : "")}
          </ul>
          <h6 className="desc-title">Pictures</h6>

            <SRLWrapper>
          <div className="flex">
              {/* <div className="container horizontal-scrollable">
                <div className="row text-center"> */}
                  {property?.mediaFiles && property?.mediaFiles.length >= 1
                    ? property?.mediaFiles.map((file, index) => {
                        return file.isImage ? (
                            <img
                              src={file.url}
                              alt=""
                              className="admin-img-field"
                            />
                        ) : null;
                      })
                    : null}
                {/* </div>
              </div> */}
          </div>
            </SRLWrapper>
          <h6 className="desc-title">Interactive 3D Tour</h6>
          <div className="flex">

            <div className="row">
              {property?.mediaFiles && property?.mediaFiles.length >= 1
                ? property?.mediaFiles.map((file, index) => {
                    return file.isVideo ? (
                      <div className="col">
                        <video
                          className="admin-img-field"
                          src={file.url}
                          controls
                        />
                      </div>
                    ) : null;
                  })
                : null}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
