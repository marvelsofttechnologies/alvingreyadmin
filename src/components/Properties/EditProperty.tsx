import React, { useEffect, useState } from "react";
import { MediaInput, PropertyInput, PropertyView } from "../../utilities/Interfaces";
import { Formik, FormikHelpers, Form, Field } from "formik";
import { toast } from "react-toastify";
import Property from "../../classes/Property";
import Dropzone from "react-dropzone";
import Spinner from "../../utilities/Spinner";
import { Statuses } from "../../utilities/Enums";
import { Editor } from "@tinymce/tinymce-react";


export default function EditProperty({
  property,
}: {
  property: PropertyView;
}) {
  console.log(property);
  const [fetchedProperty,setFetchedProperty] = useState<PropertyView>();
  const [description,setDescription] = useState(property.description);
  const [loading,setLoading] = useState(false);
  const initialValues: PropertyInput = {
    id: property?.id,
    name: property?.name,
    title: property?.title,
    address: property?.address,
    description: property?.description,
    sellMyself: property?.sellMyself,
    price: property?.price,
    numberOfBedrooms: property?.numberOfBedrooms,
    numberOfBathrooms: property?.numberOfBathrooms,
    isDraft: property?.isDraft,
    isActive: property?.isActive,
    isForRent: property?.isForRent,
    isForSale: property?.isForSale,
    propertyTypeId: property?.propertyTypeId,
    mediaFiles: [],
    state: property?.state,
    lga: property?.lga,
    area: property?.area,
    isRequest: false,
    comment: "",
    budget: "",
    statusId: property?.status == Statuses.ONSALE ? 6 : property?.status  == Statuses.SOLD ? 5 :property?.status  == Statuses.DRAFT ? 7 : 0,
  };
  const [mediaFiles,setMediaFiles] = useState<MediaInput[]>([]);
  const [previews,setPreviews] = useState<string[]>([]);
  const [videoPreviews,setVideoPreviews] = useState<string[]>([]);

  const grabUploadedFile = (uploadedFiles : any) => {
    console.log(uploadedFiles);
    extractPreviewFromFile(uploadedFiles);
    console.log({previews});
    
		uploadedFiles.forEach((file: any) => {
			const reader = new FileReader();

			reader.onabort = () => {
				console.log("Errrrrrrrrrrrrrooooooooooorrrrrrr");
			};
			reader.onerror = () => {
				console.log("Errrrrrrrrrrrrrooooooooooorrrrrrr");
			};
			reader.onload = async () => {
				// Do whatever you want with the file contents
				const binaryStr = (reader.result as string).split(',')[1];
				console.log(reader.result);
				//console.log(binaryStr);
				console.log(binaryStr);
				await composeMedia(binaryStr, file);
			};
			console.log(file);
			reader.readAsDataURL(file);
		});
	};

  const grabUploadedVideoFile = (uploadedFiles : any) => {
    console.log(uploadedFiles);
    extractPreviewFromFile(uploadedFiles,true);
    console.log({previews});
    
		uploadedFiles.forEach((file: any) => {
			const reader = new FileReader();

			reader.onabort = () => {
				console.log("Errrrrrrrrrrrrrooooooooooorrrrrrr");
			};
			reader.onerror = () => {
				console.log("Errrrrrrrrrrrrrooooooooooorrrrrrr");
			};
			reader.onload = async () => {
				// Do whatever you want with the file contents
				const binaryStr = (reader.result as string).split(',')[1];
				console.log(reader.result);
				//console.log(binaryStr);
				console.log(binaryStr);
				await composeMedia(binaryStr, file);
			};
			console.log(file);
			reader.readAsDataURL(file);
		});
	};

 const extractPreviewFromFile = async (uploadedFiles: any[],isVideo = false) => {
    var newState: string[]= []
    uploadedFiles.map(element => {
      console.log(element);
      newState.push(URL.createObjectURL(element))
    });
    if(isVideo)
    {
      setVideoPreviews([...videoPreviews,...newState])
      return;
    }
    setPreviews([...previews,...newState])
  }

	const composeMedia = async (bytes: any, file:any) => {
		var files = [...mediaFiles];
    console.log(files);
		var newMedia : MediaInput = {
			name: file.name,
			extention: getFileExtension(file.name),
			base64String: bytes,
			propertyId: 0,
			isImage:
				getFileExtension(file.name) == "jpeg" ||
				getFileExtension(file.name) == "jpg"
					? true
					: false,
			isVideo: getFileExtension(file.name) == "mp4" ? true : false,
			isDocument: getFileExtension(file.name) == "pdf" ? true : false,
		};

		files.push(newMedia);
    setMediaFiles([...files]);
	};

  const getFileExtension = (fileName : any) => {
		return fileName.split(".")[1];
	};

  const update = async (details:PropertyInput) => {
    setLoading(true);
    details.mediaFiles = mediaFiles;
    if(description){
      details.description = description;
    }
    try{
      let data = await Property.Update(details);
      if(data.status){
        toast.success(data.message);
        setLoading(false);
        window.location.reload();
        return;
      }
      toast.error(data.message);
      setLoading(false);

    }catch(error){
      console.log(error);
      setLoading(false);
      
    }
  }

 const deleteMedia =  async (id:any) => {
   try{
     const data = await Property.DeleteMedia(id);
     if(data.status){
       toast.success(data.message);
       window.location.reload();
       return;
     }

     toast.error(data.message);
   }catch (error) {
     console.error(error);
     
   }

 }

  return (
    <>
      <Formik
        initialValues={initialValues}
        enableReinitialize
        onSubmit={async (
          values: PropertyInput,
          { setSubmitting }: FormikHelpers<PropertyInput>
        ) => {
          console.log(values);
          await update(values);
        }}
      >
        <Form className="col-lg-8">
          <div className="row">
            <div className="col-lg-6">
              <div className="single-form">
                <label className="admin-form">Name</label>
                <Field
                  id="name"
                  name="name"
                  className="admin-form-field"
                  defaultValue={property?.name}
                />
              </div>
              <div className="single-form">
                <label className="admin-form">Property Title</label>
                <Field
                  id="title"
                  name="title"
                  className="admin-form-field"
                />
              </div>
              <div className="single-form">
                <label className="admin-form">No. of Bedrooms</label>
                <Field
                  id="name"
                  name="numberOfBedrooms"
                  className="admin-form-field"
                />
              </div>
              <div className="single-form">
                <label className="admin-form">No. of Bathrooms</label>
                <Field
                  id="name"
                  name="numberOfBathrooms"
                  className="admin-form-field"
                />
              </div>
              <div className="single-form">
                <label className="admin-form">Price</label>
                <Field
                  id="price"
                  name="price"
                  className="admin-form-field"
                />
              </div>
              <div className="single-form">
                <label className="admin-form">State</label>
                <Field
                  id="state"
                  name="state"
                  className="admin-form-field"
                />
              </div>
              <div className="single-form">
                <label className="admin-form">Add Images</label>
                <div className="flex">
                  {property?.mediaFiles && property?.mediaFiles?.length > 0 ?
                    property?.mediaFiles.map((file,index) => {
                      return (
                        file.isImage ?  <div className='img-trash-wrapper'>
                          <i className="fas fa-trash img-trash" onClick={async() => await deleteMedia(file.id)}></i>
                          <img src={file.url} alt='' className='admin-img-field'/>
                        </div> : null
                      )
                    })
                  : null}
                  {previews.map((preview, index) =>{
                    return <img src={preview} className="admin-img-field" alt=''/>
                  })}
                <Dropzone
						accept="image/jpeg, image/png"
						maxFiles={6}
						onDrop={(acceptedFiles) => grabUploadedFile(acceptedFiles)}
					>
						{({ getRootProps, getInputProps }) => (
							<section>
								<div {...getRootProps()} className="admin-img-field">
									<input {...getInputProps()} />
									{/* <i className="far fa-image" />
									Upload pictures */}
								</div>
							</section>
						)}
					</Dropzone>
                </div>
              </div>
              <div className="single-form">
                <label className="admin-form">Add Interactive 3D tour</label>
                <div className="flex">
                {property?.mediaFiles && property?.mediaFiles?.length > 0 ?
                    property?.mediaFiles.map((file,index) => {
                      return (
                        file.isVideo?  <div className='img-trash-wrapper'>
                          <i className="fas fa-trash img-trash" onClick={async() => await deleteMedia(file.id)}></i>
                          <video src={file.url}  className='admin-img-field' controls/>
                        </div> : null
                      )
                    })
                  : null}
                  {videoPreviews.map((preview, index) =>{
                    return <video src={preview} className="admin-img-field" />
                  })}
                <Dropzone
						accept="video/mp4"
						maxFiles={6}
						onDrop={(acceptedFiles) => grabUploadedVideoFile(acceptedFiles)}
					>
						{({ getRootProps, getInputProps }) => (
							<section>
								<div {...getRootProps()} className="admin-img-field">
									<input {...getInputProps()} />
									{/* <i className="far fa-image" />
									Upload pictures */}
								</div>
							</section>
						)}
					</Dropzone>
                </div>
              </div>
              <button className="admin-update mt-5 w-100" type="submit">{loading ? <Spinner/> : "Update"}</button>
            </div>
            <div className="col-lg-6">
              <div className="single-form">
                <label className="admin-form">Location</label>
                <Field
                  id="address"
                  name="address"
                  className="admin-form-field"
                />
              </div>
              <div className="single-form">
                <label className="admin-form">Details</label>
                <Field as={Editor}
                   init={{
                    height: 500,
                    menubar: false,
                    plugins: [
                      'advlist autolink lists link image charmap print preview anchor',
                      'searchreplace visualblocks code fullscreen',
                      'insertdatetime media table paste code help wordcount'
                    ],
                    skin:"material-classic",
                    toolbar: "bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | link",
                  }}
                  onEditorChange={(e:any) => {
                    console.log(e)
                    setDescription(e);
                }}
                  
                  apiKey={"h48cw4xuitutcnrtl0o32kl2h1u1pedw4y94bnxabwnr74dg"}
                  id="description"
                  name="description"
                  className="admin-form-field h-unset"
                  style={{height:"150px"}}
                />
              </div>
              <div className="select-box">
                <Field as='select' name="statusId">
                  <option value={0} disabled >
                    Status
                  </option>
                  <option value={6} selected={property.status == Statuses.ONSALE ? true : false} >
                    On Sale
                  </option>
                  <option value={5}  selected={property.status == Statuses.SOLD ? true : false}>
                    Sold
                  </option>
                  <option value={7}  selected={property.status == Statuses.DRAFT ? true : false}>
                    Draft
                  </option>
                </Field>
                <div className="arrow-box" />
              </div>
            </div>
          </div>
        </Form>
      </Formik>
    </>
  );
}
