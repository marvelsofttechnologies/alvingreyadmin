import React, { useState, } from "react";
import { MediaInput, PropertyInput, PropertyView } from "../../utilities/Interfaces";
import { Formik, FormikHelpers, Form, Field } from "formik";
import Dropzone from "react-dropzone";
import Property from "../../classes/Property";
import { toast } from "react-toastify";
import Spinner from "../../utilities/Spinner";

export default function AddProperty({close}: {close:any}) {
  const initialValues: PropertyInput = {
    name: '',
    title: '',
    address: '',
    description: '',
    sellMyself: false,
    price: 0,
    numberOfBedrooms: 0,
    numberOfBathrooms: 0,
    isDraft: false,
    isActive: true,
    isForRent: false,
    isForSale: true,
    propertyTypeId: 1,
    mediaFiles: [],
    state: '',
    lga: '',
    area: '',
    isRequest: false,
    comment: "",
    budget: "",
    statusId:0
  };
  const [loading,setLoading] = useState(false);
  const [mediaFiles,setMediaFiles] = useState<MediaInput[]>([]);
  const [previews,setPreviews] = useState<string[]>([]);
  const [videoPreviews,setVideoPreviews] = useState<string[]>([]);

  const grabUploadedFile = (uploadedFiles : any) => {
    console.log(uploadedFiles);
    extractPreviewFromFile(uploadedFiles);
    console.log({previews});

		uploadedFiles.forEach((file: any) => {
			const reader = new FileReader();

			reader.onabort = () => {
				console.log("Errrrrrrrrrrrrrooooooooooorrrrrrr");
			};
			reader.onerror = () => {
				console.log("Errrrrrrrrrrrrrooooooooooorrrrrrr");
			};
			reader.onload = async () => {
				// Do whatever you want with the file contents
				const binaryStr = (reader.result as string).split(',')[1];
				console.log(reader.result);
				//console.log(binaryStr);
				console.log(binaryStr);
				await composeMedia(binaryStr, file);
			};
			console.log(file);
			reader.readAsDataURL(file);
		});
	};

  const grabUploadedVideoFile = (uploadedFiles : any) => {
    console.log(uploadedFiles);
    extractPreviewFromFile(uploadedFiles,true);
    console.log({previews});

		uploadedFiles.forEach((file: any) => {
			const reader = new FileReader();

			reader.onabort = () => {
				console.log("Errrrrrrrrrrrrrooooooooooorrrrrrr");
			};
			reader.onerror = () => {
				console.log("Errrrrrrrrrrrrrooooooooooorrrrrrr");
			};
			reader.onload = async () => {
				// Do whatever you want with the file contents
				const binaryStr = (reader.result as string).split(',')[1];
				console.log(reader.result);
				//console.log(binaryStr);
				console.log(binaryStr);
				await composeMedia(binaryStr, file);
			};
			console.log(file);
			reader.readAsDataURL(file);
		});
	};

  const extractPreviewFromFile = async (uploadedFiles: any[],isVideo = false) => {
    var newState: string[]= []
    uploadedFiles.map(element => {
      console.log(element);
      newState.push(URL.createObjectURL(element))
    });
    if(isVideo)
    {
      setVideoPreviews([...videoPreviews,...newState])
      return;
    }
    setPreviews([...previews,...newState])
  }


	const composeMedia = async (bytes: any, file:any) => {
		var files: any[] = [];
    console.log(files);
		var newMedia : MediaInput = {
			name: file.name,
			extention: getFileExtension(file.name),
			base64String: bytes,
			propertyId: 0,
			isImage:
				getFileExtension(file.name) == "jpeg" ||
				getFileExtension(file.name) == "jpg"
					? true
					: false,
			isVideo: getFileExtension(file.name) == "mp4" ? true : false,
			isDocument: getFileExtension(file.name) == "pdf" ? true : false,
		};

		files.push(newMedia);
    setMediaFiles([...mediaFiles,newMedia]);
	};

  const getFileExtension = (fileName : any) => {
		return fileName.split(".")[1];
	};

  const createProperty = async(details:PropertyInput) => {
    setLoading(true);
    if(mediaFiles.length >= 0){
      details.mediaFiles = mediaFiles;
    }else{
      details.mediaFiles = undefined;
    }
    console.log({details});
    let data = await  Property.Add(details);
    if(data.status) {
      toast.success(data.message);
      await Property.List();
      close();
      setLoading(false);
      return;
    }
    toast.error(data.message);
    console.log(data);
    
  }

  return (
    <div className="container mt-4">
      <div className="user-content-box w-100">
        <div className="flex-item mb-4">
          <h5 className="modal-title">Add a New Property</h5>
          <i className="fas fa-times mod" onClick={() => close()}/>
        </div>
        <Formik
        initialValues={initialValues}
        onSubmit={async (
          values: PropertyInput,
          { setSubmitting }: FormikHelpers<PropertyInput>
        ) => {
          console.log(values);
          await createProperty(values);
        }}
      >
        <Form>
          <div className="row">
          <div className="col-lg-6">
              <div className="single-form">
                <label className="admin-form">Name</label>
                <Field
                  id="name"
                  name="name"
                  className="admin-form-field"
                />
              </div>
              <div className="single-form">
                <label className="admin-form">Property Title</label>
                <Field
                  id="title"
                  name="title"
                  className="admin-form-field"
                />
              </div>
              <div className="single-form">
                <label className="admin-form">No. of Bedrooms</label>
                <Field
                  id="name"
                  name="numberOfBedrooms"
                  className="admin-form-field"
                />
              </div>
              <div className="single-form">
                <label className="admin-form">No. of Bathrooms</label>
                <Field
                  id="name"
                  name="numberOfBathrooms"
                  className="admin-form-field"
                />
              </div>
              <div className="single-form">
                <label className="admin-form">Price</label>
                <Field
                  id="price"
                  name="price"
                  className="admin-form-field"
                />
              </div>
              <div className="single-form">
                <label className="admin-form">State</label>
                <Field
                  id="state"
                  name="state"
                  className="admin-form-field"
                />
              </div>
              <div className="single-form">
                <label className="admin-form">Add Images</label>
                <div className="flex">
                <Dropzone
						accept="image/jpeg, image/png"
						maxFiles={6}
						onDrop={(acceptedFiles) => grabUploadedFile(acceptedFiles)}
					>
						{({ getRootProps, getInputProps }) => (
							<section>
								<div {...getRootProps()} className="admin-img-field">
									<input {...getInputProps()} />
									{/* <i className="far fa-image" />
									Upload pictures */}
								</div>
							</section>
						)}
					</Dropzone>
                  {previews.map((preview, index) =>{
                    return <img src={preview} className="admin-img-field" alt=''/>
                  })}
                  
                </div>
              </div>
              <div className="single-form">
                <label className="admin-form">Add Interactive 3D tour</label>
                <div className="flex">
                {videoPreviews.map((preview, index) =>{
                    return <video src={preview} className="admin-img-field" controls/>
                  })}
                <Dropzone
						accept="video/mp4"
						maxFiles={6}
						onDrop={(acceptedFiles) => grabUploadedVideoFile(acceptedFiles)}
					>
						{({ getRootProps, getInputProps }) => (
							<section>
								<div {...getRootProps()} className="admin-img-field">
									<input {...getInputProps()} />
									{/* <i className="far fa-image" />
									Upload pictures */}
								</div>
							</section>
						)}
					</Dropzone>
                </div>
              </div>
              {/* <button className="admin-update mt-5 w-100" type="submit">Update</button> */}
            </div>
            <div className="col-lg-6">
            <div className="single-form">
                <label className="admin-form">Location</label>
                <Field
                  id="address"
                  name="address"
                  className="admin-form-field"
                />
              </div>
              <div className="single-form">
                <label className="admin-form">Area</label>
                <Field
                  id="area"
                  name="area"
                  className="admin-form-field"
                />
              </div>
              <div className="single-form">
                <label className="admin-form">Details</label>
                <Field as='textarea'
                  id="description"
                  name="description"
                  className="admin-form-field h-unset"
                  style={{height:"150px"}}
                />
              </div>
              <div className="select-box">
                <Field as='select' name="statusId">
                  <option value={0} disabled >
                    Status
                  </option>
                  <option value={6}>
                    On Sale
                  </option>
                  <option value={5}>
                    Sold
                  </option>
                  <option value={7}>
                    Draft
                  </option>
                </Field>
                <div className="arrow-box" />
              </div>
              {/* <div className="select-box">
                <select name=''>
                  <option value={0} disabled selected>
                    Status
                  </option>
                </select>
                <div className="arrow-box" />
              </div> */}
            </div>
            <div className="col-lg-12">
              <div className="flex-center">
                <button className="admin-update mt-5 mb-5" type='submit'>{loading ? <Spinner/> :  'Add'}</button>
              </div>
            </div>
          </div>
        </Form>
        </Formik>
      </div>
    </div>
  );
}
