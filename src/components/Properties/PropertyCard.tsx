import React from 'react'
import { PropertyView } from '../../utilities/Interfaces'

export default function PropertyCard({property}:{property: PropertyView}) {
    return (
        <div className="col-lg-12">
              <div className="listing-cards">
                <div className="listing-cover-img">
                  <img src="https://upload.wikimedia.org/wikipedia/commons/3/3f/Placeholder_view_vector.svg" alt="" />
                  <div className="listing-location">{property.state}</div>
                </div>
                <div className="listing-info">
                  <div className="title-group">
                    <div className="listing-title">
                      {property.name}
                    </div>
                  </div>
                  <div className="feature-group">
                    <div className="feature-sing">
                      <i className="far fa-bed" />
                      <div className="feature-title">{`${property.numberOfBedrooms} Bedrooms`}</div>
                    </div>
                    <div className="feature-sing">
                      <i className="far fa-toilet" />
                      <div className="feature-title">{`${property.numberOfBathrooms} Bathrooms`}</div>
                    </div>
                    <div className="feature-sing">
                      <i className="far fa-tags" />
                      <div className="feature-title">7 Bedrooms</div>
                    </div>
                    <div className="feature-sing">
                      <i className="far fa-award" />
                      <div className="feature-title">{property.propertyType}</div>
                    </div>
                  </div>
                </div>
                <div className="line" />
                <div className="listing-info pt-0">
                  <div className="listing-btn">
                    <button className="list-no-color-btn w-100">
                      See Details
                    </button>
                  </div>
                </div>
              </div>
            </div>
    )
}
