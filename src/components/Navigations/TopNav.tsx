import React, { useContext, useEffect, useState } from "react";
import { useLocation } from "react-router";
import { MainContext } from "../../contexts/MainContext";



export default function TopNav() {
  const {user } = useContext(MainContext).data;
  const location = useLocation();
  const [title,setTitle] = useState('');

  useEffect(() => {
    location.pathname.startsWith('/dashboard') ? setTitle('Dashboard') :location.pathname.startsWith('/realtors') ? setTitle('Realtors') : location.pathname.startsWith('/properties') ? setTitle('Properties') : location.pathname.startsWith('/payments') ? setTitle('Payments') : location.pathname.startsWith('/admin') ? setTitle('Admin') : setTitle('')
  })

  return (
<div className="top-nav">
  <div className="admin-container">
    <div className="top-nav-content">
      <h3 className="ag-page">{title}</h3>
      <div className="user-nav">
        <span className="user-greetings">{`Hello ${user?.firstName}`}</span>
        <div className="user-avatar">
          <img src="/Assets/Avatar.png" alt='' />
        </div>
        <i className="fal fa-angle-down" />
      </div>
    </div>
  </div>
</div>

);
}
